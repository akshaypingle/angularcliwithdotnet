import { AngularTestKarmaPage } from './app.po';

describe('angular-test-karma App', () => {
  let page: AngularTestKarmaPage;

  beforeEach(() => {
    page = new AngularTestKarmaPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
