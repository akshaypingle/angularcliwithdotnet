﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AngularCLIwithDotNet.Startup))]
namespace AngularCLIwithDotNet
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
